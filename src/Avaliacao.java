import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Avaliacao {

    public static void main(String[] args) {

        String path = "dataset.CSV";

        List<Pessoa> list = new ArrayList<Pessoa>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"))) {

            String line = br.readLine();
            line = br.readLine();

            while (line != null) {

                String[] elements = line.split(";");

                if (elements.length < 4) {
                    line = br.readLine();
                }
                else {
                    String nome = elements[0];
                    String sobrenome = elements[1];
                    Float peso = Float.parseFloat(elements[2].replaceAll(",", "."));
                    Float altura = Float.parseFloat(elements[3].replaceAll("," , "."));

                    if (altura > peso){
                        peso = Float.parseFloat(elements[3].replaceAll(",", "."));
                        altura = Float.parseFloat(elements[2].replaceAll("," , "."));
                    }
                    
                    Pessoa prod = new Pessoa(nome, sobrenome, peso, altura);
                    list.add(prod);

                    line = br.readLine();
                }

            }

            try (PrintWriter writer = new PrintWriter("AugustoJunior.txt")) {

                StringBuilder sb = new StringBuilder();

                for (Pessoa pessoa : list) {
                    Float imc = pessoa.getPeso()/(pessoa.getAltura()*pessoa.getAltura());
                    System.out.println(pessoa.getNome() + " " + pessoa.getSobrenome() + " " + imc);
                    sb.append(pessoa.getNome());
                    sb.append(' '); 
                    sb.append(pessoa.getSobrenome());
                    sb.append(' ');
                    sb.append(String.format("%.2f", imc));
                    sb.append("\n");
                }

                writer.write(sb.toString());

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            }

        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}