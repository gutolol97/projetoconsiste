import java.io.Serializable;
import java.util.Locale;

public class Pessoa implements Serializable {
    private static final long serialVersionUID = 1L;

    private String nome;
    private String sobrenome;
    private Float peso;
    private Float altura;

    public Pessoa() {
    }

    public Pessoa(String nome, String sobrenome, Float peso, Float altura) {
        super();
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.peso = peso;
        this.altura = altura;
    }

    public String getNome() {
        return nome.toUpperCase(Locale.ROOT).replaceAll("\\s{2,}", " ").trim();
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome.toUpperCase(Locale.ROOT).replaceAll("\\s{2,}", " ").trim();
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public Float getAltura() {
        return altura;
    }

    public void setAltura(Float altura) {
        this.altura = altura;
    }

}
